<?php
namespace Neoauto\Infrastructure\Dao\Factory;

use Neoauto\Infrastructure\Dao\DaoEloquent\AdecsysTipoTarifaDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\AvisoDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\AvisoImpresoDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\ExtracargoDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\TipoDocumentoDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\TipoPublicacionUsuarioAvisoDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\AutoDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\ColorDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\MarcaDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\ModeloDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\TipoTransmisionDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\CombustibleDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\TraccionDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\TipoTimonDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\CantPuertaDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\EnteDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\AdecsysDetalleAvisoDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\AdecsysTarifaDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\FechaCierreImpresionDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\AvisoAgregadoDao;
use Neoauto\Common\Adapter\Persistence\EloquentRepository;
use Neoauto\Infrastructure\Dao\DaoEloquent\TransaccionDao;
use Neoauto\Infrastructure\Dao\DaoEloquent\UsuarioDao;

class FactoryDao
{
    /**
     * var Neoauto\Common\Adapter\Persistence\EloquentRepository
     */
    private $repository;

    public function __construct()
    {
        //
    }

    public function getDataBase()
    {
        return null;
    }

    public function getFile()
    {
        return null;
    }

    public function getCloudWatch()
    {
        return null;
    }
}