<?php
namespace Neoauto\Infrastructure\Dao\DaoEloquent;

use Illuminate\Database\Eloquent\Model;
use Neoauto\Common\Adapter\Persistence\EloquentRepository;

class DataBaseDao extends Model
{
    protected $table      = 'npc_aviso';
    protected $primaryKey = 'IdAviso';
    public    $timestamps = false;

    public function __construct()
    {
        //parent::__construct();
    }

    public function auto()
    {
        return $this->hasOne('Neoauto\Infrastructure\Dao\DaoEloquent\AutoDao', 'IdAviso');
    }

    public function usuario()
    {
        return $this->hasOne('Neoauto\Infrastructure\Dao\DaoEloquent\UsuarioDao', 'IdUsuarioAnunciante', 'IdUsuarioAnunciante');
    }

    public function findIdAvisoUrlId($idAviso, $urlId = null)
    {
        if (!empty($urlId)) {
            return self::where('UrlId', $urlId)->first();
        }

        return self::find($idAviso);
    }
}