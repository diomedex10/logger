<?php
/**
 * Created by PhpStorm.
 * User: edson
 * Date: 1/5/17
 * Time: 12:21 PM
 */

namespace Neoauto\Domain;


interface ILog
{
    public function error();
}