<?php
namespace Neoauto\Common\Adapter\Service;

use Guzzle\Http\Client as Client;
use Neoauto\Common\Util\Configuration;

class AdapterAdecsys
{
    private $clientHttp;
    private $config;

    public function __construct()
    {
        $configTemp       = Configuration::getConfigService();
        $this->config     = $configTemp['adecsys-api'];
        $this->clientHttp = new Client($this->config['url']);
    }

    public function getClientHttp()
    {
        return $this->clientHttp;
    }

    public function getConfiguration()
    {
        return $this->config;
    }

git remote add origin ssh://git@bitbucket.org/bitbucket-personal:diomedex10/logger.git
git remote add origin git@bitbucket-personal:diomedex10/logger.git
git@bitbucket-personal:diomedex10/logger.git
}