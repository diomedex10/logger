<?php
namespace Neoauto\Common\Adapter\Persistence;

use Neoauto\Common\Util\Configuration;
use Illuminate\Database\Capsule\Manager as Capsule;
/**
 * Created by PhpStorm.
 * User: diomedes
 * Date: 11/09/16
 * Time: 8:31 PM
 */
class EloquentRepository
{
    public function __construct()
    {
        try {
            $config = Configuration::getConfigPersistence();

            if (empty($config['multidb']['default'])) {
                throw new \Exception('Configuracion de DataBase vacia');
            }

            //Creamos un nuevo objeto de tipo Capsule
            $capsule = new Capsule();
            //Indicamos en el siguiente array los datos de configuración de la BD
            $capsule->addConnection([
                'driver'    =>'mysql',
                'host'      => $config['multidb']['default']['host'],
                'database'  => $config['multidb']['default']['dbname'],
                'username'  => $config['multidb']['default']['username'],
                'password'  => $config['multidb']['default']['password'],
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ]);

            //finalmente, iniciamos Eloquent
            $capsule->bootEloquent();
        } catch (\Exception $e) {
            throw $e;
        }
    }

}