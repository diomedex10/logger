<?php
namespace Neoauto\Common\Util;
require_once __DIR__ . '/../../Lib/Spyc.php';

class Configuration
{
    public function __construct()
    {
    }

    public static function getConfig()
    {
        try {
            $reader = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $log    = $reader->fromFile(Server::getConfigEnviromentDir().'/log.config.yaml');

            return array_merge_recursive($log);

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
