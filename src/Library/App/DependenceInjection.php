<?php
namespace Neoauto\Config;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class DependenceInjection
{
    protected $container;

    public function __construct()
    {
        date_default_timezone_set("America/Lima");
        $this->container = new ContainerBuilder();
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function getDependency($service)
    {
        if ($this->container->has($service)) {
            return $this->container->get($service);
        }

        return null;
    }

    /**
     * @return \Neoauto\Infrastructure\Dao\Factory\DaoFactoryEloquent
     * @throws \Exception
     */
    public function getFactoryEloquent()
    {
        try {
            if ($this->container->has('daoFactoryEloquent')) {
                return $this->container->get('daoFactoryEloquent');
            }

            $eloquentRepository = new Definition('Neoauto\Common\Adapter\Persistence\EloquentRepository');
            $eloquentRepository->setAutowired(true);
            $this->container->setDefinition('EloquentRepository', $eloquentRepository);

            $daoFactoryEloquent = new Definition('Neoauto\Infrastructure\Dao\Factory\DaoFactoryEloquent', array(
                new Reference('EloquentRepository'),
            ));
            $daoFactoryEloquent->setAutowired(true);
            $this->container->setDefinition('daoFactoryEloquent', $daoFactoryEloquent);

            return $this->container->get('daoFactoryEloquent');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return \Neoauto\Infrastructure\Dao\Factory\DaoFactoryRedis
     * @throws \Exception
     */
    public function getFactoryRedis()
    {
        try {
            if ($this->container->has('daoFactoryRedis')) {
                return $this->container->get('daoFactoryRedis');
            }

            $redisRepository= new Definition('Neoauto\Common\Adapter\Persistence\RedisRepository');
            $redisRepository->setAutowired(true);
            $this->container->setDefinition('RedisRepository', $redisRepository);

            $daoFactoryRedis = new Definition('Neoauto\Infrastructure\Dao\Factory\DaoFactoryRedis', array(
                new Reference('RedisRepository'),
            ));
            $daoFactoryRedis->setAutowired(true);
            $this->container->setDefinition('daoFactoryRedis', $daoFactoryRedis);

            return $this->container->get('daoFactoryRedis');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return \Neoauto\Infrastructure\Dao\Factory\DaoFactorySolr
     * @throws \Exception
     */
    public function getFactorySolr()
    {
        try {
            if ($this->container->has('daoFactorySolr')) {
                return $this->container->get('daoFactorySolr');
            }

            $solrRepository= new Definition('Neoauto\Common\Adapter\Persistence\SolrRepository');
            $solrRepository->setAutowired(true);
            $this->container->setDefinition('SolrRepository', $solrRepository);

            $daoFactorySolr = new Definition('Neoauto\Infrastructure\Dao\Factory\DaoFactorySolr', array(
                new Reference('SolrRepository'),
            ));
            $daoFactorySolr->setAutowired(true);
            $this->container->setDefinition('daoFactorySolr', $daoFactorySolr);

            return $this->container->get('daoFactorySolr');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return \Neoauto\Infrastructure\Service\ServiceAdecsys
     * @throws \Exception
     */
    public function getSericeAdecsys()
    {
        try {
            if ($this->container->has('ServiceAdecsys')) {
                return $this->container->get('ServiceAdecsys');
            }

            $solrRepository= new Definition('Neoauto\Common\Adapter\Service\AdapterAdecsys');
            $solrRepository->setAutowired(true);
            $this->container->setDefinition('AdapterAdecsys', $solrRepository);

            $daoFactorySolr = new Definition('Neoauto\Infrastructure\Service\ServiceAdecsys', array(
                new Reference('AdapterAdecsys')
            ));
            $daoFactorySolr->setAutowired(true);
            $this->container->setDefinition('ServiceAdecsys', $daoFactorySolr);

            return $this->container->get('ServiceAdecsys');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}